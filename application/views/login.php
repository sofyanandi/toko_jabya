<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="<?=base_url();?>/assets/login/style.css" type="text/css" media="screen">
</head>
<body>
    <div class="konten">
        <div class="kepala">
            <div class="lock"></div>
            <h2 class="judul">Sign In</h2>
        </div>
        <div class="artikel">
            <form action="<?=base_url();?>auth/login" method="post">
                <div class="grup">
                    <label for="username">Username</label>
                    <input type="text" name="username" placeholder="Masukkan username Anda">
                </div>
                <div class="grup">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Masukkan password Anda">
                </div>
                <div class="grup">
                    <input type="submit" value="Sign In">
                </div>
                  <center><?php
					if ($this->session->flashdata('info') == true) { echo $this->session->flashdata('info');}
					?></center>            
        </form>
          
        </div>
		
    </div>
</body>
</html>