<?php
	foreach($detail_jabatan as $data) 
	{
    	$kode_jabatan	= $data->kode_jabatan;
		$nama_jabatan	= $data->nama_jabatan;
		$ket			= $data->keterangan;
    }
?>

<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3">Edit Jabatan</font></center><br>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>jabatan/editjabatan/<?=$kode_jabatan?>" method="POST";>
<table width="40%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>Kode Jabatan</td>
    <td>:</td>
    <td><input value="<?=$kode_jabatan;?>"type="text" name="kode_jabatan" id="kode_jabatan" maxlength="10" 
    value="<?= set_value('kode_jabatan');?>"></td>
  </tr>
  <tr>
    <td>Nama Jabatan</td>
    <td>:</td>
    <td><input value="<?=$nama_jabatan;?>"type="text" name="nama_jabatan" id="nama_jabatan" maxlength="50"
    value="<?= set_value('nama_jabatan');?>"></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><input value="<?=$ket;?>"type="text" name="keterangan" id="keterangan" maxlength="50"
    value="<?= set_value('keterangan');?>">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>jabatan/listjabatan""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>